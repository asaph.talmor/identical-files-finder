Use: Run ident_finder.py and provide base directory, chunk size (default = 65536)

Requested task:
"You have a filesystem, with a random set of files with random names in various places in the file system tree. The file sizes could be any size, including very large files (several Gigabytes). 
Some of these files are duplicates.
Write a python script, to output the list of all duplicate files (all duplicate files grouped together).
Try to make the program run as efficiently as possible."

How it works:
Files are grouped by size, then files with non-unique size are checked for md5 to get the lists of every group of duplicate files.