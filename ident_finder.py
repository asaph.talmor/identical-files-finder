import hashlib, os
root_dir = input("Enter base directory:\n")
CHUNK_SIZE = int(input("Enter chunk size, default = 65536 (recommended):\n") or 65536)
list_of_files = [] # used in get_identical_sizes() to loop all files
dict_of_file_sizes = {} # sent from get_identical_sizes to inverse_dict() to populate and loop a dict of identical sizes, and create list_of_same_size_files
list_of_same_size_files = [] # created using dict_of_file_sizes and looped in main() to create dict_of_file_hashes
dict_of_file_hashes = {} # sent from main to inverse_dict to populate and display duplicate files.

def md5(file):
	"""
	Returns md5 of file using specified chunk size.
	:param file: file name from list_of_same_size_files
	:type file: str
	:return: md5 hash
	:rtype: str
	"""	
	hash_md5 = hashlib.md5()
	with open(file, "rb") as f:
		for chunk in iter(lambda: f.read(CHUNK_SIZE), b""):
			hash_md5.update(chunk)
	return hash_md5.hexdigest()

def inverse_dict(dict_to_inverse):
	"""
	Returns inversed dictionary, {value: keys}.
	:param dict_to_inverse: 
	:type file: str
	:return: inverse dictionary
	:rtype: dict
	"""	
	new_dict = {}
	for item in dict_to_inverse: #create new_dict with keys as values
		new_key = dict_to_inverse[item]
		new_value = item
		if new_key in new_dict.keys():
			new_dict[new_key].append(new_value)
		else:	
			new_dict[new_key] = [new_value]
	return new_dict

def get_identical_sizes(root_dir):
	"""
	Lists all files and returns dict_of_file_sizes.
	:param root_dir: From user input 
	:type root_dir: str
	:return: inverse dictionary
	:rtype: dict
	"""	
	#creates list_of_files
	for subdir, dirs, files in os.walk(root_dir):
		for file in files:
			list_of_files.append(os.path.join(subdir, file))
	#get file sizes and create size dict
	for file in list_of_files:
		dict_of_file_sizes[file] = os.path.getsize(file)
	return inverse_dict(dict_of_file_sizes)

def main():
	"""
	Main function. Loops files with non-unique sizes to get md5 for each and determine if an identical file exists.
	:return: None
	"""	
	#create list_of_same_size_files to check md5 of
	for size in get_identical_sizes(root_dir):
		set_of_files = get_identical_sizes(root_dir)[size]
		if len(set_of_files) > 1: #exclude files with unique size
			for file in set_of_files:
				list_of_same_size_files.append(file) #create list of files to check md5 of
	for file in list_of_same_size_files: #create dict_of_file_hashes
		dict_of_file_hashes[file] = md5(file)
	for hash in inverse_dict(dict_of_file_hashes): #inverse dict_of_file_hashes
		if len(inverse_dict(dict_of_file_hashes)[hash]) > 1:
			print("\nIdentical files found:", inverse_dict(dict_of_file_hashes)[hash], end = "\n\n")

main()
		